Было использовано:

Linux 16.04 (Runtu)
Symfony 3.4.4 (основные бандлы SonataAdminBundle, FOSUserBundle, OldSoundRabbitMqBundle)
Bootstrap #3.3.7 (подключен с помощью загрузчика пакетов Bower)

=======================================================================
	**************** Установка приложения ********************
=======================================================================
1. Склонируйте с удаленного репозиторя файлы:

git clone https://ndz2000@bitbucket.org/ndz2000/testwork.git

Должна создаться папка testwork c первичной структурой Symfony проекта. 


2. Зайдите в корень проекта и выполните загрузку ядра проекта:

composer install

В ходе установки потребуется указать параметры для базы данных 
и отправки почты:

database_host (127.0.0.1): 
database_port (null): 
database_name (null): symfony 			- имя вашей базы данных
database_user (null): root 				- пользователь
database_password (null): 1 			- пароль
mailer_transport (null): gmail 			- если вы используете google-email
mailer_host (null): 127.0.0.1
mailer_user (null): *здесь укажите ваш e-mail*
mailer_password (null): *здесь укажите ваш пароль от e-mail*
secret (ThisTokenIsNotSoSecretChangeIt): 

Предпологается, что в локальной версие apache настроин, вот пример моей
конфигурации:

<VirtualHost *:80>
ServerName testwork
ServerAlias testwork
ServerAdmin webmaster@localhost
DocumentRoot /var/www/testwork/web/
ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

3. Установить права на файлы и папки (пункт 3 https://symfony.com/doc/3.4/setup/file_permissions.html)
 Выполнить поочередно командыв  корне проекта:

 HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
 sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
 sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var

4. Подключить bootstrap и jquery. 
 * Нужно Установить Bower(для возможности работы с Bower предпологается, что Node.js и npm установлены) командой:
 
bower install

В итоге должны создаться папки  web/assets/vendor/bootstrap и web/assets/vendor/jquery с файлами
 
5. Настройка бызы данных.
Выполните поочередно команды для создания базы данных и её структуры:

php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force

В итоге должна быть создана база данных со структурой 


-----------------------------------------------------------------------
С этого момента основной функционал сайта становится доступным
и можно пройти в корень сайта и при нажатии на ссылку Click here,
Вам предложат авторизироваться. Так же здесь находится ссылка 
Reset Password по которой можно сбросить пароль.
----------------------------------------------------------------------- 
6. Регистрация:
На странице нужно пройти по ссылке Register и пройти регистрацию,
после чего, в случае успешной регистрации вы сможете пройти по ссылкам
и в итоге попасть на страницу /books_page 
, которя доступна для зарегестрированных пользователей. На этой странице
в панели навигации досупна форма поиска книг.

По умолчанию пользователю даётся роль ROLE_USER
 
 
7. Управление сайтом (админка):
Для того чтоб стала доступна администраторская часть пользователю 
(/admin), следует повысить его права до уровня ROLE_SUPER_ADMIN. 
Делается это с помощью консольных команд FOSUser бандла, командой

php bin/console fos:user:promote username ROLE_SUPER_ADMIN

где username - имя пользователя.
Теперь, если Вы были залогинены, следует перелогиниться и снова зайти.
(для того, чтоб разлогиниться, следует пройти по ссылке /logout). 
Другие консольные команды для управления пользователями, предоставляемые
FOSUserBundle
https://symfony.com/doc/current/bundles/FOSUserBundle/command_line_tools.html

======================================================================
 ******** RabbitMQ - отправка сообщений на email ******************
======================================================================
 0. Работа с очередями проводится с помощью бандла OldSoundRabbitMqBundle

 1. Зайдите на 
http://localhost:15672 - интерфейс, логин: guest, пароль: guest


 2. В вкладке Overview (первая) выбираем Import definitions (в самом низу)
и импортируем настройки файл rabbit_roman_2019-1-24.json либо создайте 
чистый json файл и поместите в нем следующую строчку:

{"rabbit_version":"3.6.15","users":[{"name":"guest","password_hash":"9KF3gJYQCBxFFKActD52ckDT/OT8eYnMtvd1H/y9rGtQZG+m","hashing_algorithm":"rabbit_password_hashing_sha256","tags":"administrator"}],"vhosts":[{"name":"/"}],"permissions":[{"user":"guest","vhost":"/","configure":".*","write":".*","read":".*"}],"parameters":[],"global_parameters":[{"name":"cluster_name","value":"rabbit@roman"}],"policies":[],"queues":[{"name":"book.send","vhost":"/","durable":true,"auto_delete":false,"arguments":{}}],"exchanges":[{"name":"my_send","vhost":"/","type":"direct","durable":true,"auto_delete":false,"internal":false,"arguments":{}}],"bindings":[{"source":"my_send","vhost":"/","destination":"book.send","destination_type":"queue","routing_key":"","arguments":{}}]} 

После этого сохраните этот файл и импортируйте его.


 3. Для функционирования получения сообщений выполняем последовательно 
 консольные команды из корня проекта:

php bin/console rabbitmq:setup-fabric

- создает обменник (exchange)

php bin/console rabbitmq:consumer book_handler

- включаем Consumer

 4. Добавляем через админку книгу:
 
 * В админке переходим во вкладку Book. Выбираем пункт Add new.
 
 * В открывшемся окне нужно заполнить данные и нажать любую из кнопок
	Create...
	
 5. Далее сообщение попадает в очередь - это можно отследить через 
	интерфейс http://localhost:15672/#/queues/%2F/book.send
    И в итоге оно должно оказаться у Вас в почтовом ящике :)
 ======================================================================
 P.S.
 
	В задании я в некоторых местах решил выйти за рамки тестового задания,
 так например, я мог упростить себе некоторые задачи. Однако, например,
 я реализовал связи ManyToMany между авторами и книгами вместо OneToMany,
 что в следствии усложнило настройку SonataAdmin и поиск. Далее, мог 
 сделать более простой фильтр поиска, который искал бы по одному из 
 полей, используя полное слово. Вместо этого я сделал поиск по частичным
 словам, из всех сущностей, а так же конкатенировал два поля из сущности
  Author и сделал поиск по частично совпадающему имени или фамилии.
  

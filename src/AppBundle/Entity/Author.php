<?php
namespace AppBundle\Entity;

use AppBundle\Entity\Book;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AuthorRepository")
 */
class Author
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;


    /**
     * @var string
     *
     * @ORM\Column(name="sur_name", type="string", length=255)
     */
    private $surName;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Book", mappedBy="authors", fetch="EXTRA_LAZY")
     */
    protected $books;
    
    
    public function __construct()
    {
        $this->books = new ArrayCollection();
    }
    
    public function getBooks()
    {
        return $this->books;
    }
    
    
    public function addBook(Book $book) 
    {
        $this->books[] = $book;
        
        return $this;
    }
    
    
    public function removeBook(Book $book)
    {
        $this->books->removeElement($book);
        $book->setGenre(null);
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Author
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surName
     *
     * @param string $surName
     *
     * @return Author
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;

        return $this;
    }

    /**
     * Get surName
     *
     * @return string
     */
    public function getSurName()
    {
        return $this->surName;
    }
    
    
    public function __toString() 
    {
        return $this->getFullName();
    }
    
    
    public function getFullName()
    {
        return $this->getFirstName()." ".$this->getSurName();
    }
}


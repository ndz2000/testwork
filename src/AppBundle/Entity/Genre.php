<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Genre
 *
 * @ORM\Table(name="genre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GenreRepository")
 */
class Genre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=255, unique=true)
     */
    private $genre;
    
    
    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Book", mappedBy="genre"))
     */
    protected $books;

    
    public function __construct()
    {
        $this->books = new ArrayCollection();
        
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set genre
     *
     * @param string $genre
     *
     * @return Genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }
    
    
    public function addBooks(Book $book)
    {
        if ($this->books->contains($book)) {
            return;
        }
        
        $this->books[] = $book;
        $book->setGenre($this);
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }
        
    
    /**
     * @return Collection|Books[]
     */
    public function getBooks()
    {
        return $this->books;
    }
    
    public function __toString() 
    {
        return $this->getGenre();
    }
}

<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Genre;
use AppBundle\Entity\Author;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookRepository")
 */
class Book
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="isbn", type="string", length=25, unique=true)
     */
    private $isbn;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Genre", inversedBy="books"))
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     */
    protected $genre;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Author", inversedBy="books")
     * @ORM\JoinTable(name="author_book")
     */
    protected $authors;
    
    
    function __construct() 
    {
        $this->authors = new ArrayCollection(); 
    }

    
    public function addAuthor(Author $author)
    {
        $authorId = $author->getId();
        $allAuthors = $this->authors->toArray();
        
        foreach($allAuthors as $key => $value) {
            if ($authorId == $value->getId()) {
                return;
            }
        }
        $this->authors = $author;
        
        return $this;
    }
    
    
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
    }
    
    
    public function getAuthors()
    {
        return $this->authors;
    }
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     *
     * @return Book
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }
    

    /**
     * Set genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return Book
     */
    public function setGenre(Genre $genre)
    {
        $this->genre = $genre;
    }

    /**
     * Get genre
     *
     * @return \AppBundle\Entity\Genre
     */
    public function getGenre()
    {
        return $this->genre;
    }
    

    /**
     * Set title
     *
     * @return string
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}

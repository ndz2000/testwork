<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\{DatagridMapper, ListMapper};
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Entity\{Author, Genre};

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;



class BookAdmin extends AbstractAdmin
{
    private $producer;
    
    
    public function setProducer(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('isbn')
            ->add('genre')
            ->add('authors');
    }
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('isbn')
            ->add('genre', null, ['label' => 'Genre'])
            ->add('authors', null, ['label' => 'Author'])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('isbn')
            ->add('authors', 'sonata_type_model', [
                'label'    => 'Author',
                'multiple' => true,
                'expanded' => true,
                'class'    => Author::class,
                'property' => 'fullName',
            ])
            ->add('genre', 'sonata_type_model', [
                'label'    => 'Genre',
                'multiple' => false,
                'expanded' => true,
                'class'    => Genre::class,
                'property' => 'genre',
            ]);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('isbn')
            ->add('authors', 'sonata_type_model', [
                'label' => 'Author',
            ])
            ->add('genre', 'sonata_type_model', [
                'label' => 'Genre',
            ]);
    }
    
    
    public function postPersist($book)
    {
        $bookArr = $this->getAsArray($book);
        
        $this->producer->publish(json_encode($bookArr));        
    }
    
    
    public function getAsArray($book)
    {
        return [
            'id'        => $book->getId(),
            'date'      => date('Y-m-d H:i:s'),
            'title'     => $book->getTitle(),
            'genre'     => $book->getGenre()->getGenre(),
            'author'    => $book->getAuthors()[0]->getFullName(),
        ];
    }
}

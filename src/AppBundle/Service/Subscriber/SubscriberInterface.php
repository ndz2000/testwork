<?php
namespace AppBundle\Service\Subscriber;


interface SubscriberInterface
{
    public function getSubscribers();
}
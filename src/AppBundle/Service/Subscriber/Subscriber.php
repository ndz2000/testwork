<?php
namespace AppBundle\Service\Subscriber;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\User;


class Subscriber implements SubscriberInterface
{
    private $em;
    
    public function __construct(EntityManagerInterface $em) 
    {
        $this->em = $em;
    }
    
    
    public function getSubscribers()
    {
        $subscribers = $this->em->getRepository(User::class)->findByEnabled('1');
        
        return $subscribers;
    }
}


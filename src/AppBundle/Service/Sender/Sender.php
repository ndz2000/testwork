<?php
namespace AppBundle\Service\Sender;

use Swift_Mailer;
use Twig_Environment;
use AppBundle\Service\Sender\SenderInterface;
use AppBundle\Service\Subscriber\SubscriberInterface;
use Psr\Log\LoggerInterface;


class Sender implements SenderInterface
{
    private $twig;
    private $mailerService;
    private $logger;
    private $subscriberService;
    private $senderName;

    /**
     * Constructor
     */
    public function __construct(
        Twig_Environment        $twig,
        Swift_Mailer            $mailerService,
        LoggerInterface         $logger,
        SubscriberInterface     $subscriberService,
                                $senderName
    )
    {
	$this->twig              = $twig;
        $this->mailerService     = $mailerService;
        $this->logger            = $logger;
        $this->subscriberService = $subscriberService;
        $this->senderName        = $senderName;
    }
    
    
    public function sendToAddressees($message)
    {
        $senderName = $this->senderName;    
        
        $subscribers = $this->subscriberService->getSubscribers();
        
        foreach ($subscribers as $subscriber) {
            
            $sendStatus = $this->sendMail($senderName, $subscriber->getEmail(), $message);
            
            $status = $sendStatus ? "yes" : "no";
            
            $this->logger->info("Message sent to {$subscriber->getId()}: "."$status");
        }
    }

    
    public function sendMail($senderName, $emailUser, $message)
    {
	$mailMessage = \Swift_Message::newInstance()
            ->setSubject('New Book')
            ->setFrom($senderName)
            ->setTo($emailUser)
            ->setBody(
                $this->twig->render(
                    'AppBundle:Email:email.html.twig', [
                        'title'     => $message['title'],
                        'author'    => $message['author'],
                        'genre'     => $message['genre'],
                        'subscriber'=> $emailUser,
                    ]
                ),
                'text/html'
            );
            
        return $this->mailerService->send($mailMessage);
    }

}
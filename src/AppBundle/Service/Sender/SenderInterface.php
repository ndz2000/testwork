<?php
namespace AppBundle\Service\Sender;


interface SenderInterface
{
    /**
     * Send mesasge to the users.
     */
    public function sendToAddressees($message);
}
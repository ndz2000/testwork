<?php
namespace AppBundle\Service\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use AppBundle\Service\Sender\SenderInterface;


class BookConsumer implements ConsumerInterface
{
    private $multipleSender;
    
    public function __construct(SenderInterface $multipleSender)
    {
        $this->multipleSender = $multipleSender;
    }


    /**
     * @var AMQPMessage $msg
     * @return void
     */
    public function execute(AMQPMessage $msg)
    {
        $messageBody = json_decode($msg->getBody(), true);

        $this->multipleSender->sendToAddressees($messageBody);
    }

}

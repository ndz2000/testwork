<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\{TextType, SubmitType};
use Symfony\Component\Validator\Constraints as Assert;


class SearchBookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('genre', TextType::class, ['label' => 'Genre:', 'required' => false])
               ->add('book_title', TextType::class, ['label' => ' Book Title:', 'required' => false])
               ->add('author', TextType::class, ['label' => ' Author:', 'required' => false]);
    }
   
    
    public function getBlockPrefix()
    {
	return 'search_book_form';
    }
}
<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Book;
use AppBundle\Form\Type\SearchBookType;


class FilterController extends Controller
{
    /**
     * @Route("/books_page", name="books_page")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        
        $books = $em->getRepository(Book::class)->findAll();
        
        return $this->render('AppBundle:Filter:books.html.twig', [
            'books' => $books,           
        ]);
    }
    
    
    /**
     * @Route("/searchBar", name="search_book")
     */
    public function searchBarAction()
    {
        $searchBookForm = $this->createForm(SearchBookType::class);
        
        return $this->render('AppBundle:Form:searchBookForm.html.twig', [
            'form' => $searchBookForm->createView(),
        ]);
    }
    
    
    /**
     * @Route("/search_book_result", name="handle_search_book")
     */
    public function handleSearchForm(Request $request)
    {   
        $form = $this->createForm(SearchBookType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $data = $form->getData();

            $em = $this->getDoctrine();
            
            $books = $em->getRepository(Book::class)
                ->searchBookByParam($data['genre'], $data['book_title'], $data['author']);
            
            
            return $this->render('AppBundle:Filter:books.html.twig', [
                'books' => $books,
            ]);
        }
    }

}
